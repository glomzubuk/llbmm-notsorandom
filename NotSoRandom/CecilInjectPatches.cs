﻿using System;
using System.Collections.Generic;
using System.Linq;
using LLHandlers;
using UnityEngine;

namespace NotSoRandom
{
    public static class CecilInjectPatches
    {
        public static bool PlayerDetermineCharacter_Prefix(ALDOKEMAOMB _this, Character[] __0)
        {
            if (__0 != null)
                return true;

            if (_this.GAFCIHKIGNM && _this.IMJLOPPPIJM && _this.LALEEFJMMLH != Character.NONE)
            {
                Character[] nsrSkips = NotSoRandom.GetSkipCharacters();
                if (nsrSkips.Length > 0)
                {
                    _this.LALEEFJMMLH = _this.HGPNPNPJBMK(nsrSkips);
                    _this.AIINAIDBHJI = _this.GLCHOPBLHEB(false);
                    return true;
                }
            }
            return false;
        }

        public static bool GetRandomStage_Prefix(out Stage __result, bool only_unlocked, StageRandom which)
        {
            List<Stage> list = (which != StageRandom.ANY_2D) ? StageHandler.stages3d : StageHandler.stages2d;
            if (only_unlocked)
            {
                if (JOMBNFKIHIC.GDNFJCCCKDM)
                {
                    list = EPCDKLCABNC.JKGLOFIECCP(list);
                }
                else
                {
                    list = EPCDKLCABNC.JKGLOFIECCP(list);
                }
            }
            if (list.Count == 0 && which == StageRandom.ANY_2D)
            {
                __result = StageHandler.GetRandomStage(only_unlocked, StageRandom.ANY_3D);
                return true;
            }
            List<Stage> nsrStages = NotSoRandom.GetValidStages();
            if (nsrStages.Count > 0)
            {
                list = list.Intersect(nsrStages).ToList();
            }
            __result = list[UnityEngine.Random.Range(0, list.Count)];
            return true;
        }
    }
}
