﻿using System.IO;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using LLHandlers;

namespace NotSoRandom
{
    public class NotSoRandom : MonoBehaviour
    {
        public static ModMenuIntegration MMI = null;
        public static NotSoRandom instance = null; 
        private static List<Character> characterList = null;
        private const string charKeyHeader = "(bool)EnableChar";
        private const string stageKeyHeader = "(bool)EnableStage";

        public static void Initialize()
        {
            GameObject gameObject = new GameObject("NotSoRandom");
            instance = gameObject.AddComponent<NotSoRandom>();
            DontDestroyOnLoad(gameObject);
        }

        private void Update()
        {
            if (MMI == null) { MMI = gameObject.AddComponent<ModMenuIntegration>(); }
            else if (MMI.configBools.Count == 0 && MMI != null && LLHandlers.StageHandler.stagesAll != null)
            {
                if(LLModMenu.ModMenu.Instance != null)
                {
                    InitCharactersConfig();
                    InitStagesConfig();
                    MMI.InitConfig();
                    MMI.ReadIni();
                }
            }
        }


        public static Character[] GetSkipCharacters()
        {
            List<Character> charList = new List<Character>();
            if (MMI != null && MMI.configBools != null)
            {
                Debug.Log("Char?");
                foreach (var pair in MMI.configBools.Where(
                    (charConfig) => charConfig.Key.StartsWith(charKeyHeader) && charConfig.Value == "false"))
                {
                    string charName = pair.Key.Substring(charKeyHeader.Length);
                    Debug.Log(pair.Key);
                    Debug.Log(charName);

                    Character toAdd;
                    switch (charName)
                    {
                        case "KID":
                            toAdd = Character.KID; break;
                        case "ROBOT":
                            toAdd = Character.ROBOT; break;
                        case "CANDY":
                            toAdd = Character.CANDY; break;
                        case "BOOM":
                            toAdd = Character.BOOM; break;
                        case "CROC":
                            toAdd = Character.CROC; break;
                        case "PONG":
                            toAdd = Character.PONG; break;
                        case "BOSS":
                            toAdd = Character.BOSS; break;
                        case "COP":
                            toAdd = Character.COP; break;
                        case "ELECTRO":
                            toAdd = Character.ELECTRO; break;
                        case "SKATE":
                            toAdd = Character.SKATE; break;
                        case "GRAF":
                            toAdd = Character.GRAF; break;
                        case "BAG":
                            toAdd = Character.BAG; break;
                        default:
                            toAdd = Character.NONE; break;
                    }
                    if (toAdd != Character.NONE)
                        charList.Add(toAdd);
                }
            }
            return charList.ToArray();
        }

        public static List<Stage> GetValidStages()
        {
            if (MMI != null && MMI.configBools != null)
            {
                Debug.Log("Stage?");
                List<Stage> stageList = new List<Stage>();
                foreach (var pair in MMI.configBools.Where(
                    (stageConfig) => stageConfig.Key.StartsWith(stageKeyHeader) && stageConfig.Value == "true"))
                {
                    string stageName = pair.Key.Substring(stageKeyHeader.Length);
                    Debug.Log(pair.Key);
                    Debug.Log(stageName);

                    Stage toAdd;
                    switch (stageName)
                    {
                        case "CONSTRUCTION":
                            toAdd = Stage.CONSTRUCTION; break;
                        case "FACTORY":
                            toAdd = Stage.FACTORY; break;
                        case "FACTORY_2D":
                            toAdd = Stage.FACTORY_2D; break;
                        case "JUNKTOWN":
                            toAdd = Stage.JUNKTOWN; break;
                        case "NONE":
                            toAdd = Stage.NONE; break;
                        case "OUTSKIRTS":
                            toAdd = Stage.OUTSKIRTS; break;
                        case "OUTSKIRTS_2D":
                            toAdd = Stage.OUTSKIRTS_2D; break;
                        case "POOL":
                            toAdd = Stage.POOL; break;
                        case "POOL_2D":
                            toAdd = Stage.POOL_2D; break;
                        case "ROOM21":
                            toAdd = Stage.ROOM21; break;
                        case "ROOM21_2D":
                            toAdd = Stage.ROOM21_2D; break;
                        case "SEWERS":
                            toAdd = Stage.SEWERS; break;
                        case "SEWERS_2D":
                            toAdd = Stage.SEWERS_2D; break;
                        case "STADIUM":
                            toAdd = Stage.STADIUM; break;
                        case "STREETS":
                            toAdd = Stage.STREETS; break;
                        case "STREETS_2D":
                            toAdd = Stage.STREETS_2D; break;
                        case "SUBWAY":
                            toAdd = Stage.SUBWAY; break;
                        case "SUBWAY_2D":
                            toAdd = Stage.SUBWAY_2D; break;
                        default:
                            toAdd = Stage.NONE; break;
                    }
                    if(toAdd != Stage.NONE)
                        stageList.Add(toAdd);
                }
                Debug.Log("Stage count: " + stageList.Count);
                return stageList;
            }
            else
            {
                return LLHandlers.StageHandler.stagesAll;
            }
        }

        public static List<T> GetEnumValues<T>() where T : new()
        {
            T valueType = new T();
            return typeof(T).GetFields()
                .Select(fieldInfo => (T)fieldInfo.GetValue(valueType))
                .Distinct()
                .ToList();
        }

        private static void InitCharactersConfig()
        {
            if (characterList == null)
                characterList = GetEnumValues<Character>();

            MMI.AddToWriteQueue("(header)h1", "Characters");
            foreach (Character character in characterList)
            {
                if (character != Character._MAX_NORMAL && character != Character.NONE &&
                 character != Character.RANDOM && character != Character.DUMMY)
                {
                    MMI.AddToWriteQueue(charKeyHeader + character.ToString(), "true");
                }
            }
        }
        private void InitStagesConfig()
        {
            MMI.AddToWriteQueue("(header)h2", "Stages");
            foreach (Stage stage in LLHandlers.StageHandler.stagesAll)
            {
                if(stage != Stage.BALLROOM && stage != Stage.TITLE && stage != Stage.START && stage != Stage.NONE)
                {
                    MMI.AddToWriteQueue(stageKeyHeader + stage.ToString(), "true");
                }
            }
        }
    }
}