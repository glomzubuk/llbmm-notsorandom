# ⚠️ ***THIS MOD IS NOT TOURNAMENT LEGAL*** ⚠️

A mod that allows to pick only the characters/stages you want to from selecting random.
Everything can be configured in mod menu, though i'd like to make some ergonomic improvements on that side. 


As of now, character restriction only affects the local players, and only if they are hosts.
I am planning on fixing this, at least partially.


Online, you may land on a stage you banned.
This is because the game randomly chooses from each players selection. 
So if your opponents picked a stage you restricted, or landed on it with their own random, it still affects the chances of it being chosen.
For now, there is no plans to fix this.


As with all mods that injects code with LLBMM, it is impossible to uninstall properly.
Uninstall all mods, make backups of your non-LLBMM mods, verify files through steam then reinstall all mods.
